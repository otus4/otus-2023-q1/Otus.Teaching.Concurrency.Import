﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class XmlRangeLoader : IDataLoader
    {
        private class ThrStartObj
        {
            public int startCustomer;
            public int customerCount;
            public List<Customer> customerList;
            public Mutex accCustomerList;
            public string[] source;
        }

        private readonly string _dataFile;
        private int custForThr = 5000;
        private readonly int linesPerCustomer = 6;
        private readonly int linesSkipHead = 3;
        private readonly int linesSkipTail = 2;
        private readonly bool takeThrFromPool = false;
        private string[] source;

        public XmlRangeLoader(string dataFile, bool takeThrFromPool)
        {
            this._dataFile = dataFile;
            this.takeThrFromPool = takeThrFromPool;
        }

        public void LoadData()
        {
            var stw = new Stopwatch();
            stw.Start();
            var mt = new Mutex();
            var cl = new List<Customer>();
            var custCnt = (long)(File.ReadLines(_dataFile).Count() - linesSkipHead - linesSkipTail) / linesPerCustomer;
            if (custForThr > custCnt) custForThr = (int)custCnt;
            source = new string[custCnt];
            source = File.ReadAllLines(_dataFile);

            var thrCnt = custCnt / custForThr;
            if (takeThrFromPool)
            {
                int workedThred = (int)thrCnt;
                using (ManualResetEvent resetEvent = new ManualResetEvent(false))
                {
                    for (int i = 0; i < thrCnt; i++)
                    {
                        var thrSO = new ThrStartObj()
                        {
                            startCustomer = i * custForThr,
                            customerCount = custForThr,
                            accCustomerList = mt,
                            customerList = cl,
                            source = source
                        };
                        ThreadPool.QueueUserWorkItem(
                            new WaitCallback(x =>
                            {
                                RangeLoadWorker(thrSO);
                                if (Interlocked.Decrement(ref workedThred) == 0) resetEvent.Set();
                            }));
                    }
                    resetEvent.WaitOne();
                }
            }
            else
            {
                var thrList = new List<Thread>();
                for (int i = 0; i < thrCnt; i++)
                {
                    var thrSO = new ThrStartObj()
                    {
                        startCustomer = i * custForThr,
                        customerCount = custForThr,
                        accCustomerList = mt,
                        customerList = cl,
                        source = source
                    };
                    var thr = new Thread(RangeLoadWorker);
                    thrList.Add(thr);
                    thr.Start(thrSO);
                }
                foreach (var thr in thrList) thr.Join();
            }
            stw.Stop();
            var methDescr = takeThrFromPool ? "pool" : "native";
            Console.WriteLine($"{methDescr}, колич. потоков - {thrCnt}, время на обр. файла - {stw.Elapsed.TotalMilliseconds} ms");
        }

        private void RangeLoadWorker(object so)
        {
            if (!File.Exists(_dataFile)) return;
            List<string> custlines = new List<string>
            {
                "<Customers>"
            };

            int start = (so as ThrStartObj).startCustomer * linesPerCustomer + linesSkipHead;
            int end = (so as ThrStartObj).startCustomer * linesPerCustomer + linesSkipHead + (so as ThrStartObj).customerCount * linesPerCustomer;
            for (int i = start; i < end; i++) custlines.Add((so as ThrStartObj).source[i]);

            if (custlines[custlines.Count - 1].Trim() != "</Customers>") custlines.Add("</Customers>");
            if (custlines[custlines.Count - 2].Trim() == "</Customers>") custlines.RemoveAt(custlines.Count - 1);
            var parser = new XmlParser(ref custlines);
            var custList = parser.Parse();
            var dwriter = new CustomerRepository();
            foreach (var customer in custList) dwriter.AddCustomer(customer);
        }
    }
}
