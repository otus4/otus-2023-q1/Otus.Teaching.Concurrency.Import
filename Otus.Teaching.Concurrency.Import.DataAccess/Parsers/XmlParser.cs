﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly List<string> _lines;
        public XmlParser(ref List<string> lines)
        {
            _lines = lines;
        }
        public List<Customer> Parse()
        {
            List<Customer> result = new List<Customer>();
            Customer cs = null;

            foreach (var s in _lines)
            {
                var token = getTag(s.Trim(), out string value);

                switch (token)
                {
                    case "<Customer>":
                        {
                            cs = new Customer();
                            break;
                        }
                    case "<Id>":
                        {
                            cs.Id = Convert.ToInt32(value);
                            break;
                        }
                    case "<Phone>":
                        {
                            cs.Phone = value;
                            break;
                        }
                    case "<Email>":
                        {
                            cs.Email = value;
                            break;
                        }
                    case "<FullName>":
                        {
                            cs.FullName = value;
                            break;
                        }
                    case "</Customer>":
                        {
                            result.Add(cs);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }

            }
            return result;
        }
        string getTag(string source, out string value)
        {
            int i = 0;
            value = "";
            string sr = "";
            while (source[i] != '>') { sr += source[i]; i++; }
            sr += source[i];
            i++;
            if (i >= source.Length) { return sr; }
            while (source[i] != '<') { value += source[i]; i++; }
            return sr;
        }
    }
}