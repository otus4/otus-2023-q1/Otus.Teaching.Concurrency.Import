using System;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly int retryCount = 5;
        readonly string connectionString = "Host=localhost;Port=5431;Username=postgres;Password=postgres;Database=postgres";

        public void AddCustomer(Customer customer)
        {
            var rcnt = retryCount;
            var conn = new Npgsql.NpgsqlConnection(connectionString);
            while (rcnt > 0)
            {
                conn.Open();
                if (conn.State == System.Data.ConnectionState.Open) break;
                Thread.Sleep(300);
                rcnt--;
            }
            if (conn.State != System.Data.ConnectionState.Open)
            {
                Console.WriteLine($"can't get pg connection for storing customer, customer.id - {customer.Id}");
                return;
            }

            var cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO customers (id, fname, email, phone) VALUES(:id, :fname, :email, :phone)";
            cmd.Parameters.Add(new Npgsql.NpgsqlParameter("id", customer.Id));
            cmd.Parameters.Add(new Npgsql.NpgsqlParameter("fname", customer.FullName));
            cmd.Parameters.Add(new Npgsql.NpgsqlParameter("email", customer.Email));
            cmd.Parameters.Add(new Npgsql.NpgsqlParameter("phone", customer.Phone));
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}